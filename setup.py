#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
Redmine stats for everyone!
"""
__author__ = u"Rafał Selewońko <rafal@selewonko.com>"

import os

from setuptools import setup

version = __import__('redminestats').__version__

# allow setup.py to be run from any path and open files
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

with open('README.rst') as readme:
    README = readme.read()

with open('requirements.txt') as requirements:
    REQUIREMENTS = map(str.strip, requirements.readlines())


setup(
    name='RedmineStats',
    version=version,
    author=u'Rafał Selewońko',
    author_email='rafal@selewonko.com',
    description=__doc__,
    url='http://bitbucket.org/seler/redminestats',
    packages=['redminestats'],
    include_package_data=True,
    license='MIT',
    entry_points={'console_scripts': [
        'rstats = redminestats.commandline:main',
    ]},
    long_description=README,
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Topic :: Internet :: WWW/HTTP',
    ],
    install_requires=REQUIREMENTS,
)
