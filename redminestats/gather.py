#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
This module does that.
"""
__author__ = u"Rafał Selewońko <rafal@selewonko.com>"

import time

from redmine import Redmine
from redminestats.models import IssueState

PARAMETERS = {
    'issue_id': 'id',
    'project': 'project',
    'tracker': 'tracker',
    'status': 'status',
    'priority': 'priority',
    'version': 'version',
    'assignee': 'assigned_to',
    'author': 'author',
}


class Gather(object):

    """Docstring for Gather.

    """

    def __init__(self, redmine_url, redmine_key=None, redmine_version=None,
                 redmine_username=None, redmine_password=None, projects=None,
                 versions=None, users=None):
        self._redmine_url = redmine_url
        self._redmine_key = redmine_key
        self._redmine_version = redmine_version
        self._redmine_username = redmine_username
        self._redmine_password = redmine_password
        self._projects = projects
        self._versions = versions
        self._users = users
        self._timestamp = int(time.time())

    @property
    def redmine(self):
        if not hasattr(self, '_redmine'):
            self._redmine = Redmine(self._redmine_url, key=self._redmine_key, ver=self._redmine_version)
        return self._redmine

    def get_filters(self):
        return {
            'project_id': 'reachmd',
            'subproject_id': '*',
            'status_id': '*',
        }

    def get_issues(self):
        redmine = self.redmine
        filters = self.get_filters()
        limit = 100
        offset = 0
        proceed = True
        issues = []
        while proceed:
            proceed = False
            i = redmine.issue.filter(limit=limit, offset=offset, **filters)
            if i:
                proceed = True
                offset += limit
                issues.extend(i)

        return issues

    def serialize_issue(self, issue):
        i = {}
        for pk, pv in PARAMETERS.items():
            try:
                i[pk] = getattr(issue, pv).name
            except AttributeError:
                i[pk] = getattr(issue, pv, None)

        i['timestamp'] = self._timestamp

        return IssueState(**i)

    def serialize(self, issues):
        for issue in issues:
            yield self.serialize_issue(issue)

    def gather(self):
        issues = self.get_issues()
        self.save(self.serialize(issues))

    def save(self, issues):
        from redminestats.database import Session
        session = Session()
        session.add_all(issues)
        session.commit()
