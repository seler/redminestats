#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
TODO
RedmineStats is a tool for gathering and presenting info about your Redmine instance.
"""
__author__ = u"Rafał Selewońko <rafal@selewonko.com>"

# https://www.python.org/dev/peps/pep-0008/#version-bookkeeping
# https://www.python.org/dev/peps/pep-0440/
__version__ = '0.1'
