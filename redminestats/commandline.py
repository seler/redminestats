#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
This module contains wrappers for command line.
"""
__author__ = u"Rafał Selewońko <rafal@selewonko.com>"


import argparse

from redminestats.database import Session, engine
from redminestats.gather import Gather
from redminestats.generators import (PriorityInVersionGenerator,
                                     StatusInVersionGenerator,
                                     TrackerInVersionGenerator,
                                     GroupedStatusGenerator)
from redminestats.models import Base, IssueState


def gather(args):
    config = {
        'redmine_url': args.redmine_url,
        'redmine_key': args.redmine_key,
        'redmine_version': args.redmine_version,
        # 'redmine_username': args.redmine_username,
        # 'redmine_password': args.redmine_password,
        'projects': args.projects,
        'versions': args.versions,
        'users': args.users,
    }

    g = Gather(**config)
    g.gather()


def createdb(args):
    Base.metadata.create_all(engine)


def cleardb(args):
    session = Session()
    session.query(IssueState).delete()
    session.commit()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, default=None)
    subparsers = parser.add_subparsers(title='commands', description='description')

    # start gather command
    gather_parser = subparsers.add_parser('gather')
    gather_parser.add_argument('--redmine-url', type=str, required=True)
    gather_parser.add_argument('--redmine-key', type=str, default=None, nargs='?')
    gather_parser.add_argument('--redmine-version', type=str, default=None, nargs='?')
    # gather_parser.add_argument('--redmine-username', type=str, default=None, nargs='?')
    # gather_parser.add_argument('--redmine-password', type=str, default=None, nargs='?')
    gather_parser.add_argument('--projects', type=str, default=None, nargs='+')
    gather_parser.add_argument('--versions', type=str, default=None, nargs='+')
    gather_parser.add_argument('--users', type=str, default=None, nargs='+')
    gather_parser.set_defaults(func=gather)
    # end gather command

    # start createdb command
    createdb_parser = subparsers.add_parser('createdb')
    createdb_parser.set_defaults(func=createdb)
    # end createdb command

    # start cleardb command
    cleqardb_parser = subparsers.add_parser('cleardb')
    cleqardb_parser.set_defaults(func=cleardb)
    # end cleardb command

    # start generate command
    generate_parser = subparsers.add_parser('generate')
    generate_parser.add_argument('repository', default='.', nargs='?')

    def generate(args):
        # following required for python3
        # python2 prints error "too few arguments" in ArgumentParsers.parse_args
        if hasattr(args, 'generate_func'):
            args.generate_func(args)
        else:
            parser.print_help()

    generate_parser.set_defaults(func=generate)

    generate_subparsers = generate_parser.add_subparsers(title='plots', description='descr')

    PriorityInVersionGenerator.setup_argparser(generate_subparsers)
    StatusInVersionGenerator.setup_argparser(generate_subparsers)
    TrackerInVersionGenerator.setup_argparser(generate_subparsers)
    GroupedStatusGenerator.setup_argparser(generate_subparsers)
    # end generate command

    args = parser.parse_args()

    # following required for python3
    # python2 prints error "too few arguments" in ArgumentParsers.parse_args
    if hasattr(args, 'func'):
        args.func(args)
    else:
        parser.error('specify command')
