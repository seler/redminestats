#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
This module does that.
"""
__author__ = u"Rafał Selewońko <rafal@selewonko.com>"

import datetime

from redminestats.database import Session
from redminestats.models import IssueState
from sqlalchemy import func


class Generate(object):

    def __init__(self):
        pass

    def generate(self):
        """
        Number of not resolved tickets by priority
        """
        statuses = ['New', 'Estimate time', 'Planning', 'In Progress', 'Feedback']

        filters = [
            IssueState.status.in_(statuses),
            IssueState.version.like('v1.9'),
        ]

        session = Session()

        query = session.query(func.count(IssueState.priority), IssueState.priority, IssueState.timestamp)
        for filter in filters:
            query = query.filter(filter)
        query = query.group_by(IssueState.timestamp, IssueState.priority).all()

        session.close()
        import collections

        by_priority = collections.OrderedDict()
        by_priority["Immediate"] = {'color': '#800000', 'data': {}}
        by_priority["Urgent"] = {'color': '#CC0000', 'data': {}}
        by_priority["High"] = {'color': '#FF9900', 'data': {}}
        by_priority["Normal"] = {'color': '#0033CC', 'data': {}}
        by_priority["Low"] = {'color': '#006600', 'data': {}}
        for value, priority, timestamp in query:
            for p in by_priority.keys():
                if timestamp not in by_priority[p]['data']:
                    by_priority[p]['data'][timestamp] = 0

            by_priority[priority]['data'][timestamp] = value

        import matplotlib.pyplot as plt
        import matplotlib.dates as mdates

        years = mdates.DayLocator()   # every year
        months = mdates.HourLocator()  # every month
        yearsFmt = mdates.DateFormatter('%d-%m-%Y')

        fig, ax = plt.subplots()

        for priority in by_priority:
            timestamps, values = zip(*sorted(by_priority[priority]['data'].items(), key=lambda x: x[0]))
            dates = [datetime.datetime.fromtimestamp(ts) for ts in timestamps]
            ax.plot(
                dates,
                values,
                linestyle='solid',
                marker='.',
                color=by_priority[priority]['color'],
                label=priority,
                markersize=8)

        # format the ticks
        ax.xaxis.set_major_locator(years)
        ax.xaxis.set_major_formatter(yearsFmt)
        ax.xaxis.set_minor_locator(months)

        datemin = datetime.datetime.fromtimestamp(min(timestamps) - (3600 * 12))
        datemax = datetime.datetime.fromtimestamp(max(timestamps) + (3600 * 12))
        ax.set_xlim(datemin, datemax)
        # ax.set_ylim(0, max(values) + 5)

        ax.format_xdata = mdates.DateFormatter('%Y-%m-%d')
        ax.grid(True)

        # rotates and right aligns the x labels, and moves the bottom of the
        # axes up to make room for them
        fig.autofmt_xdate()

        handles, labels = ax.get_legend_handles_labels()
        legend = ax.legend(handles, labels,
                           bbox_to_anchor=(0, 1.01, 1, 1), ncol=5, loc=3, borderaxespad=0., mode='expand')
        # legend.get_frame().set_alpha(0.1)

        plt.savefig('asd.png')
