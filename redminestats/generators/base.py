#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
This module does that.
"""
__author__ = u"Rafał Selewońko <rafal@selewonko.com>"


import datetime
import os

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
from sqlalchemy import func

from redminestats.database import Session
from redminestats.models import IssueState

import argparse


def date(s):
    try:
        return datetime.datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


class Generator(object):

    commandline_name = 'priority-in-version'

    def __init__(self, options):
        self.options = options

    def get_title(self):
        return self.options.get('title')

    @classmethod
    def get_generate_function(cls, generator):

        def func(args):
            options = generator.parse_args(args)
            g = generator(options)
            return g.generate()

        return func

    @classmethod
    def get_possible_optional_options(cls):
        return {
            'version': {'nargs': 1, 'required': True},
            'title': {'nargs': 1},
            'filename': {'nargs': 1, 'required': True},
            'range': {'nargs': 2, 'type': date},
            'drawrange': {'default': False, 'action': 'store_true'},
        }

    def get_range(self):
        daterange = self.options.get('range')
        if daterange:
            datefrom, dateto = daterange
            day = (3600 * 24) - 1
            return int(datefrom.strftime("%s")), int(dateto.strftime("%s")) + day
        return None, None

    @classmethod
    def setup_argparser(cls, parser):
        subparser = parser.add_parser(cls.commandline_name)
        subparser.set_defaults(generate_func=cls.get_generate_function(cls))
        for option_key, option_kwargs in cls.get_possible_optional_options().items():
            subparser.add_argument("--{}".format(option_key), **option_kwargs)

    @classmethod
    def parse_args(cls, args):
        options = {}
        for option_key, option_kwargs in cls.get_possible_optional_options().items():
            option = getattr(args, option_key, (None))
            if 'nargs' in option_kwargs:
                if option_kwargs['nargs'] == 1:
                    if option is not None:
                        option = option[0]
                    else:
                        option = None

            clean = getattr(cls, 'clean_%s' % option_key, None)
            if clean:
                option = clean(option)

            options[option_key] = option
        return options

    def get_filters(self):
        filters = [
            IssueState.version.like(self.options['version']),
        ]

        datefrom, dateto = self.get_range()
        if datefrom:
            filters.append(IssueState.timestamp >= datefrom)
        if dateto:
            filters.append(IssueState.timestamp <= dateto)
        return filters

    @property
    def grouper(self):
        return IssueState.priority

    def _get_query(self):
        return self.session.query(func.count(self.grouper), self.grouper, IssueState.timestamp)

    def get_query(self):
        query = self._get_query()

        filters = self.get_filters()
        for filter in filters:
            query = query.filter(filter)

        group_by = self.get_group_by()
        if group_by:
            query = query.group_by(*group_by)

        return query

    def get_group_by(self):
        return (IssueState.timestamp, self.grouper)

    def start_session(self):
        self.session = Session()

    def close_session(self):
        self.session.close()

    def get_data(self):

        # required to get data from db
        self.start_session()

        query = self.get_query()

        # force getting data from db
        data = list(query)

        # no longer needed
        self.close_session()
        return data

    def group_data(self, data):
        grouped = {}

        for value, grouper, timestamp in data:
            # make sure that grouper is in dict
            if grouper not in grouped:
                grouped[grouper] = {}

            # make sure that all groupers have this time stamp
            # need to do this to show that key has value of zero
            for p in grouped.keys():
                if timestamp not in grouped[p]:
                    grouped[p][timestamp] = 0

            # replace value of current grouper
            grouped[grouper][timestamp] = value

        return grouped

    def draw_plot(self, ax, by_priority):
        for priority in by_priority:
            timestamps, values = zip(*sorted(by_priority[priority].items(), key=lambda x: x[0]))
            dates = [datetime.datetime.fromtimestamp(ts) for ts in timestamps]
            ax.plot(
                dates,
                values,
                linestyle='solid',
                label=priority)

    def get_grouped_data(self):
        self._raw_data = self.get_data()
        return self.group_data(self._raw_data)

    def set_time_axis(self, ax):
        # format the ticks
        hour = mdates.HourLocator()
        ax.xaxis.set_minor_locator(hour)

        day = mdates.DayLocator()
        ax.xaxis.set_major_locator(day)

        day_format = mdates.DateFormatter('%d-%m-%Y')
        ax.xaxis.set_major_formatter(day_format)

        min_date = self.get_min_date()
        max_date = self.get_max_date()
        ax.set_xlim(min_date, max_date)

    time_margin = (3600 * 12)

    def _get_minormax_date(self, minormax):
        drawrange = self.options.get('drawrange')
        if drawrange:
            date = getattr(self, '_get_%s_date_from_range' % minormax)()
        else:
            date = getattr(self, '_get_%s_date_from_data' % minormax)()
        return date

    def get_min_date(self):
        return self._get_minormax_date('min')

    def get_max_date(self):
        return self._get_minormax_date('max')

    def _get_min_date_from_data(self):
        timestamps = list(zip(*self._raw_data))[2]
        return datetime.datetime.fromtimestamp(min(timestamps) - self.time_margin)

    def _get_max_date_from_data(self):
        timestamps = list(zip(*self._raw_data))[2]
        return datetime.datetime.fromtimestamp(max(timestamps) + self.time_margin)

    def _get_min_date_from_range(self):
        # make sure its `datetime` object
        date, _ = self.get_range()
        return datetime.datetime.fromtimestamp(date - self.time_margin)

    def _get_max_date_from_range(self):
        # make sure its `datetime` object
        _, date = self.get_range()
        return datetime.datetime.fromtimestamp(date + self.time_margin)

    def set_values_axis(self, ax):
        ax.set_ylim(0, None)

    def generate(self):

        fig, ax = plt.subplots()

        grouped_data = self.get_grouped_data()

        self.draw_plot(ax, grouped_data)

        self.set_time_axis(ax)
        self.set_values_axis(ax)

        ax.format_xdata = mdates.DateFormatter('%Y-%m-%d')
        ax.grid(True)

        # rotates and right aligns the x labels, and moves the bottom of the
        # axes up to make room for them
        fig.autofmt_xdate()

        handles, labels = ax.get_legend_handles_labels()

        from matplotlib.font_manager import FontProperties

        font = FontProperties()
        font.set_size('small')

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), prop=font)

        title = self.get_title()
        if title is not None:
            plt.title(title)

        plt.savefig(self.get_filename())

    def get_filename(self):
        filename = self.options['filename']
        filename = os.path.expanduser(filename)
        return filename
