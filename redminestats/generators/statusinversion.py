#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
This module does that.
"""
__author__ = u"Rafał Selewońko <rafal@selewonko.com>"

from redminestats.generators.base import Generator
from redminestats.models import IssueState


class StatusInVersionGenerator(Generator):

    commandline_name = 'status-in-version'

    @property
    def grouper(self):
        return IssueState.status
