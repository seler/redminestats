#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
This module does that.
"""
__author__ = u"Rafał Selewońko <rafal@selewonko.com>"

from redminestats.generators.base import Generator
from redminestats.models import IssueState


class GroupedStatusGenerator(Generator):

    commandline_name = 'grouped-status'

    @property
    def grouper(self):
        return IssueState.status

    @classmethod
    def get_possible_optional_options(cls):
        options = Generator.get_possible_optional_options()
        options['group'] = {'nargs': '+', 'required': True}
        return options

    def get_data(self):
        data = super(GroupedStatusGenerator, self).get_data()

        for group in self.options['group']:
            data = self.get_grouped_statuses(group, data)

        return data

    def get_grouped_statuses(self, group, data):
        groupees, grouper = group.split('>')
        groupees = list(map(str.lower, map(str.strip, groupees.split(','))))
        grouper = grouper.strip()

        group_data = {}
        new_data = []

        for count, status, timestamp in data:
            if status.lower() in groupees:
                if timestamp not in group_data:
                    group_data[timestamp] = count
                else:
                    group_data[timestamp] += count
            else:
                new_data.append((count, status, timestamp))

        for timestamp, count in group_data.items():
            new_data.append((count, grouper, timestamp))

        return new_data
