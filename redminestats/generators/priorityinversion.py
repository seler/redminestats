#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
This module does that.
"""
__author__ = u"Rafał Selewońko <rafal@selewonko.com>"

from redminestats.generators.base import Generator
from redminestats.models import IssueState


class PriorityInVersionGenerator(Generator):

    def get_filters(self):
        filters = super(PriorityInVersionGenerator, self).get_filters()
        statuses = ['New', 'Estimate time', 'Planning', 'In Progress', 'Feedback']
        filters.append(IssueState.status.in_(statuses))
        return filters
