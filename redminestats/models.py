import datetime

from sqlalchemy import Column, Integer, Sequence, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class IssueState(Base):
    __tablename__ = 'issuestate'

    id = Column(Integer, Sequence('issuestate_id_seq'), primary_key=True)
    issue_id = Column(String)
    project = Column(String)
    tracker = Column(String)
    status = Column(String)
    priority = Column(String)
    version = Column(String)
    assignee = Column(String)
    author = Column(String)
    timestamp = Column(Integer)

    def __repr__(self):
        return "<IssueState(issue_id={}, timestamp={})>".format(self.issue_id, self.get_datetime())

    def get_datetime(self):
        if self.timestamp:
            return datetime.datetime.fromtimestamp(self.timestamp)
        else:
            return datetime.datetime.now()
