from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

engine = create_engine('sqlite:////home/seler/.redminestatsdb', echo=False)

Session = sessionmaker(bind=engine)
