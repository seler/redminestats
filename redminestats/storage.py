#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
This module does that.
"""
__author__ = u"Rafał Selewońko <rafal@selewonko.com>"

import json
import bz2
import os


class Storage(object):
    pass


class JSONBZ2FileSystemStorage(Storage):

    def __init__(self):
        self._directory = os.path.expanduser('~/.redminestats/storage')
        try:
            os.makedirs(self._directory)
        except OSError:
            pass

    def save(self, filename, data):
        with open(os.path.join(self._directory, filename), 'w') as fp:
            fp.write(bz2.compress(json.dumps(data)))

    def load(self, filename):
        with open(os.path.join(self._directory, filename), 'r') as fp:
            return json.loads(bz2.decompress(fp.read()))


def get_storage():
    return JSONBZ2FileSystemStorage()
